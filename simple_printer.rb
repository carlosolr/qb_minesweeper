# Printer to help unit tests
class SimplePrinter

  def to_string(board_state)
    board_format = [
      #unknown_cell
      '#',
      #clear_cell
      '.',
      #mine
      '*',
      #flag
      'F'
    ]

    result = ''
    board_state.rows.each do |board_state_row|
      board_state_row.cols.each do |cell|
        if cell.is_unknow
          result = result + board_format[0]
        elsif cell.is_clear
          if cell.nearby_mines > 0
            result = result + "#{cell.nearby_mines}"
          else
            result = result + board_format[1]
          end
        elsif cell.is_mine
          result+= board_format[2]
        else
          result = result + board_format[3]
        end
        result = result + ' '
      end
      result = result + "\n"
    end
    result + "\n"
  end

  def print(board_state)
    puts to_string board_state
  end

end