require File.dirname(__FILE__) + '/minesweeper'
require File.dirname(__FILE__) + '/simple_printer'

print 'Informe o Tamanho (Linhas X Colunas) e quantidade de minas:'
rows, cols, mines = gets.split

board = Minesweeper.new Integer(rows), Integer(cols), Integer(mines)
SimplePrinter.new.print board.board_state
while board.still_playing
  print 'Jogada (x y): '
  x, y = gets.split
  if board.play Integer(x), Integer(y)
    SimplePrinter.new.print board.board_state
  end
end

if board.victory
  p 'Venceu ^^b'
else
  p 'Derrota =('
  SimplePrinter.new.print board.board_state
end