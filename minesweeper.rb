require 'ostruct'

# represents the game
class Minesweeper

  def initialize(num_rows, num_cols, num_mines)
    @still_playing = true
    @victory = false
    @num_cols = num_cols
    @num_rows = num_rows
    @cells = []
    @mines = []
    @free_cells = num_cols * num_rows - num_mines

    #create cells
    for i in 0..num_rows-1
      col = []
      for j in 0..num_cols-1
        col.push MinesweeperCell.new(i, j)
      end
      @cells.push col
    end

    # check valid game
    total_cell = num_rows * num_cols
    if num_mines >= total_cell
      raise 'Too much mines!!!'
    end

    # put mines
    mines_positions = (1..total_cell).to_a.shuffle.slice(0, num_mines)
    mines_positions.each do |position|
      target_row = 0
      target_col = 0
      count = 0
      while count != position
        target_col = 0
        for i in 0..num_cols-1
          count +=1
          if count == position
            break
          end
          target_col+=1
        end
        if count != position
          target_row+=1
        end
      end
      cell = @cells[target_row][target_col]
      cell.set_mine(true)
      @mines.push(cell)
    end
  end

  def victory
    @victory
  end

  def still_playing
    @still_playing
  end

  def play(x, y)
    if x > @num_rows or y > @num_cols or x < 0 or y < 0
      raise "Invalid Position, min: 0.0, max: #{@num_rows}.#{@num_cols}"
    end
    cell_at_position = @cells[x][y]

    # invalid move
    if cell_at_position.is_checked
      return false
    end

    cell_at_position.set_checked(true)
    cell_at_position.set_unknow(false)
    if cell_at_position.is_mine
      @still_playing = false
      @victory = false
      discover_others_mines
      return false
    end
    check_around(cell_at_position)
    if @free_cells == 0
      convert_mines_to_flag
      @still_playing = false
      @victory = true
    end
    true
  end

  def board_state(x_ray = false)
    board = OpenStruct.new
    board.rows = []
    @cells.each do |cell_row|
      row = OpenStruct.new
      row.cols = []
      cell_row.each do |cell_col|
        #create cellOutPut from CellCol
        cell_out_put = OpenStruct.new
        cell_out_put.is_unknow = cell_col.is_unknow
        cell_out_put.is_clear = cell_col.is_clear
        cell_out_put.is_mine = cell_col.is_mine
        cell_out_put.nearby_mines = cell_col.nearby_mines

        if x_ray and cell_out_put.is_unknow && cell_out_put.is_mine
          cell_out_put.is_unknow = false
        end

        row.cols.push cell_out_put
      end
      board.rows.push row
    end
    board
  end

  private
  def convert_mines_to_flag
    @mines.each do |mine|
      @cells[mine.row][mine.col].set_mine(false)
      @cells[mine.row][mine.col].set_unknow(false)
      @cells[mine.row][mine.col].set_clear(false)
    end
  end

  def discover_others_mines
    @mines.each do |mine|
      @cells[mine.row][mine.col].set_unknow(false)
    end
  end

  def check_around(cell)
    if cell.is_mine || cell.is_clear
      return
    end
    x = cell.row
    y = cell.col
    # NW N NE
    # W  0  E
    # SW S SE
    n_x, n_y = x-1, y
    ne_x, ne_y = x-1, y+1
    e_x, e_y = x, y+1
    se_x, se_y = x+1, y+1
    s_x, s_y = x+1, y
    sw_x, sw_y = x+1, y-1
    w_x, w_y = x, y-1
    nw_x, nw_y = x-1, y-1

    around_cells = []
    if n_x >= 0
      mark_cell_is_around(n_x, n_y, around_cells)
    end
    if ne_x >= 0 and ne_y < @num_cols
      mark_cell_is_around(ne_x, ne_y, around_cells)
    end
    if e_y < @num_cols
      mark_cell_is_around(e_x, e_y, around_cells)
    end
    if se_x < @num_rows and se_y < @num_cols
      mark_cell_is_around(se_x, se_y, around_cells)
    end
    if s_x < @num_rows
      mark_cell_is_around(s_x, s_y, around_cells)
    end
    if sw_x < @num_rows and sw_y >= 0
      mark_cell_is_around(sw_x, sw_y, around_cells)
    end
    if w_y >= 0
      mark_cell_is_around(w_x, w_y, around_cells)
    end
    if nw_x >= 0 and nw_y >= 0
      mark_cell_is_around(nw_x, nw_y, around_cells)
    end
    around_mines = 0
    around_cells.each do |around_cell|
      if around_cell.is_mine
        around_mines+=1
      end
    end
    cell.set_unknow(false)
    cell.set_clear(true)
    @free_cells-=1
    cell.set_nearby_mines(around_mines)
    if around_mines == 0
      around_cells.each do |around_cell|
        check_around(around_cell)
      end
    end
  end

  def mark_cell_is_around(x, y, around_cells)
    cell_at_position = @cells[x][y]
    unless cell_at_position.is_checked
      around_cells.push(cell_at_position)
    end
  end

  protected
  class MinesweeperCell
    attr_accessor(:row, :col)

    def initialize(x, y)
      @is_mine = false
      @is_clear = false
      @is_unknow = true
      @is_checked = false
      @nearby_mines = 0

      @row = x
      @col = y
    end

    def is_unknow
      @is_unknow
    end

    def is_clear
      @is_clear
    end

    def is_mine
      @is_mine
    end

    def is_checked
      @is_checked
    end

    def nearby_mines
      @nearby_mines
    end

    def set_mine(is_mine)
      @is_mine = is_mine
      itself
    end

    def set_clear(is_clear)
      @is_clear = is_clear
      @is_checked = is_clear
      itself
    end

    def set_checked(is_ckecked)
      @is_ckecked = is_ckecked
      itself
    end

    def set_unknow(is_unknow)
      @is_unknow = is_unknow
      itself
    end

    def set_nearby_mines(mines)
      @nearby_mines = mines
      itself
    end
  end
end