require File.dirname(__FILE__) + '/minesweeper'

class CustomMinesweeper < Minesweeper
  def set_board(cells)
    @cells = cells
  end
  def set_mines(mines)
    @mines = mines
  end
end