# Printer for system integration
class JSONPrinter

  def print(board_state)
    result = '{"rows": ['
    board_state.rows.each_with_index do |board_state_row, i|
      result+= '{"cols": ['
      board_state_row.cols.each_with_index do |cell, j|
        result+= "{\"is_unknow\": #{cell.is_unknow},"
        result+= "\"is_clear\": #{cell.is_clear},"
        result+= "\"is_mine\": #{cell.is_mine},"
        result+= "\"nearby_mines\": #{cell.nearby_mines}}"
        if j != board_state_row.cols.length-1
          result+= ','
        end
      end
      result+= ']}'
      if i != board_state.rows.length-1
        result+= ','
      end
    end
    result + ']}'
  end

end