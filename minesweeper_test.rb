require File.dirname(__FILE__) + '/custom_minesweeper'
require File.dirname(__FILE__) + '/json_printer'
require 'test/unit'
require 'ostruct'

class MinesweeperTest < Test::Unit::TestCase

  def setup
    @game = CustomMinesweeper.new(3, 3, 2)
    @custom_cells = []
    for i in 0..2
      cols = []
      for j in 0..2
        cols.push(Minesweeper::MinesweeperCell.new(i, j))
      end
      @custom_cells.push(cols)
    end
    mines = []
    mines.push @custom_cells[0][0].set_mine(true)
    mines.push @custom_cells[1][2].set_mine(true)
    @game.set_board(@custom_cells)
    @game.set_mines(mines)
  end

  def test_initial_board_state
    assert_equal(build_initial_state([[0, 0], [1, 2]]), @game.board_state)
  end

  def test_win_game
    estado_inicial = build_initial_state([[0, 0], [1, 2]])

    assert_true @game.play 0, 1
    estado_inicial.rows[0].cols[1].is_unknow = false
    estado_inicial.rows[0].cols[1].is_clear = true
    estado_inicial.rows[0].cols[1].nearby_mines = 2
    assert_equal(estado_inicial, @game.board_state)
    assert_true @game.still_playing

    assert_true @game.play 2, 0
    estado_inicial.rows[2].cols[0].is_unknow = false
    estado_inicial.rows[2].cols[0].is_clear = true
    estado_inicial.rows[1].cols[0].is_unknow = false
    estado_inicial.rows[1].cols[0].is_clear = true
    estado_inicial.rows[1].cols[0].nearby_mines = 1
    estado_inicial.rows[2].cols[1].is_unknow = false
    estado_inicial.rows[2].cols[1].is_clear = true
    estado_inicial.rows[2].cols[1].nearby_mines = 1
    estado_inicial.rows[1].cols[1].is_unknow = false
    estado_inicial.rows[1].cols[1].is_clear = true
    estado_inicial.rows[1].cols[1].nearby_mines = 2
    assert_equal(estado_inicial, @game.board_state)
    assert_true @game.still_playing

    assert_true @game.play 0, 2
    estado_inicial.rows[0].cols[2].is_unknow = false
    estado_inicial.rows[0].cols[2].is_clear = true
    estado_inicial.rows[0].cols[2].nearby_mines = 1
    assert_equal(estado_inicial, @game.board_state)
    assert_true @game.still_playing

    assert_true @game.play 2, 2
    estado_inicial.rows[2].cols[2].is_unknow = false
    estado_inicial.rows[2].cols[2].is_clear = true
    estado_inicial.rows[2].cols[2].nearby_mines = 1
    estado_inicial.rows[0].cols[0].is_unknow = false
    estado_inicial.rows[0].cols[0].is_mine = false
    estado_inicial.rows[0].cols[0].is_clear = false
    estado_inicial.rows[1].cols[2].is_unknow = false
    estado_inicial.rows[1].cols[2].is_mine = false
    estado_inicial.rows[1].cols[2].is_clear = false
    assert_equal(estado_inicial, @game.board_state)
    assert_false @game.still_playing
    assert_true @game.victory
  end

  def test_move_cell_already_checked
    estado_inicial = build_initial_state([[0, 0], [1, 2]])

    assert_true @game.play 0, 1
    estado_inicial.rows[0].cols[1].is_unknow = false
    estado_inicial.rows[0].cols[1].is_clear = true
    estado_inicial.rows[0].cols[1].nearby_mines = 2
    assert_equal(estado_inicial, @game.board_state)
    assert_true @game.still_playing

    assert_false @game.play 0, 1
    assert_equal(estado_inicial, @game.board_state)
    assert_true @game.still_playing
  end

  def test_move_cell_with_mine
    estado_inicial = build_initial_state([[0, 0], [1, 2]])

    assert_false @game.play 0, 0
    estado_inicial.rows[0].cols[0].is_unknow = false
    estado_inicial.rows[0].cols[0].is_mine = true
    estado_inicial.rows[1].cols[2].is_unknow = false
    estado_inicial.rows[1].cols[2].is_mine = true
    assert_equal(estado_inicial, @game.board_state)
    assert_false @game.still_playing
    assert_false @game.victory
  end

  def test_json_printer
    json_expected = '{"rows": [{"cols": [{"is_unknow": false,"is_clear": false,"is_mine": true,"nearby_mines": 0},{"is_unknow": true,"is_clear": false,"is_mine": false,"nearby_mines": 0},{"is_unknow": true,"is_clear": false,"is_mine": false,"nearby_mines": 0}]},{"cols": [{"is_unknow": true,"is_clear": false,"is_mine": false,"nearby_mines": 0},{"is_unknow": true,"is_clear": false,"is_mine": false,"nearby_mines": 0},{"is_unknow": false,"is_clear": false,"is_mine": true,"nearby_mines": 0}]},{"cols": [{"is_unknow": true,"is_clear": false,"is_mine": false,"nearby_mines": 0},{"is_unknow": true,"is_clear": false,"is_mine": false,"nearby_mines": 0},{"is_unknow": true,"is_clear": false,"is_mine": false,"nearby_mines": 0}]}]}'
    assert_equal(json_expected, JSONPrinter.new.print(@game.board_state true))
  end

  # AUXILIARES
  def build_initial_state(mines = [])
    board = OpenStruct.new
    board.rows = []
    for i in 0..2
      row = OpenStruct.new
      row.cols = []
      for j in 0..2
        cell = OpenStruct.new
        cell.is_unknow = true
        cell.is_clear = false
        cell.is_mine = false
        cell.nearby_mines = 0
        row.cols.push(cell)
      end
      board.rows.push(row)
    end
    mines.each do |mine|
      board.rows[mine[0]].cols[mine[1]].is_mine = true
    end
    board
  end

end